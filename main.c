#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <libnet.h>
#include <stdbool.h>


void bredo_generate(int fd, FILE* output);

int main(int argc, char *argv[]) {
  if (argc != 3) {
    printf("Error: wrong input: %s %s %s\n", argv[0], argv[1], argv[2]);
    printf("Usage: %s <file to sort> <output file>\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  int fd = open(argv[1], O_RDWR, O_CREAT);
  if (fd == -1) {
    perror("Error opening file for reading");
    exit(EXIT_FAILURE);
  }

  FILE *output = fopen(argv[2], "w");
  if (output == NULL) {
    perror("Error opening file for reading");
    exit(EXIT_FAILURE);
  }

  bredo_generate(fd, output);

  close(fd);
  fclose(output);

  return 0;
}

size_t SpliteToSentence(int fd, unsigned char ***strings);
int Comp1(const void *aa, const void *bb);
int Comp2(const void *aa, const void *bb);


//!
//! \param fd input file
//! \param output file that contains three copies of the incoming file. first - same,
//! second - order by left to right, third - order by right to left
void bredo_generate(const int fd, FILE* output) {
  unsigned char **strings = NULL;
  size_t j = SpliteToSentence(fd, &strings);

  for (int i = 0; i < j; i++) {
    size_t z = 0;
    while (strings[i][z] != '\n' && strings[i][z] != '\0') {
      fprintf(output, "%c", strings[i][z]);
      z++;
    }
    fprintf(output, "\n");
  }
  fprintf(output, "---------------------------------------------------------\n");

  qsort(strings, (size_t) (j), sizeof(char **), Comp1);
  for (int i = 0; i < j; i++) {
    size_t z = 0;
    while (strings[i][z] != '\n' && strings[i][z] != '\0') {
      fprintf(output, "%c", strings[i][z]);
      z++;
    }
    fprintf(output, "\n");
  }
  fprintf(output, "---------------------------------------------------------\n");

  qsort(strings, (size_t) (j), sizeof(char **), Comp2);
  for (int i = 0; i < j; i++) {
    size_t z = 0;
    while (strings[i][z] != '\n' && strings[i][z] != '\0') {
      fprintf(output, "%c", strings[i][z]);
      z++;
    }
    fprintf(output, "\n");
  }
}

bool IsChar(char letter) {
  if (('a' <= letter && 'z' >= letter) || (('A' <= letter && 'Z' >= letter))) {
    return true;
  }
  return false;
}

/**
 *
 * @param [in] a first string
 * @param [in] b second string
 * @return a < b ? a : b
 */
int Comp1(const void *aa, const void *bb) {
  const char *a = *(char **) aa;
  const char *b = *(char **) bb;

  int end_a = 0;
  int end_b = 0;

  while (a[end_a] != '\n' && a[end_a] != '\0') {
    end_a++;
  }
  while (b[end_b] != '\n' && b[end_b] != '\0') {
    end_b++;
  }

  int i = 0;
  int j = 0;

  while (i < end_a && j < end_b) {
    if (!IsChar(a[i])) {
      i++;
      continue;
    }
    if (!IsChar(b[j])) {
      j++;
      continue;
    }

    if (a[i] < b[j]) {
      return false;
    }
    if (a[i] > b[j]) {
      return true;
    }
    i++;
    j++;
  }
  if (i > 0) {
    return true;
  } else {
    return false;
  }

}

/**
 *
 * @param [in] aa first string
 * @param [in] bb second string
 * @return aa < bb ? aa : bb, if right to left comparison
 */

int Comp2(const void *aa, const void *bb) {
  const char *a = *(char **) aa;
  const char *b = *(char **) bb;

  int i = 0;
  int j = 0;

  while (a[i] != '\n' && a[i] != '\0') {
    i++;
  }
  while (b[j] != '\n' && b[j] != '\0') {
    j++;
  }

  i--;
  j--;

  while (i >= 0 && j >= 0) {
    if (!IsChar(a[i])) {
      i--;
      continue;
    }
    if (!IsChar(b[j])) {
      j--;
      continue;
    }

    if (a[i] < b[j]) {
      return false;
    }
    if (a[i] > b[j]) {
      return true;
    }
    i--;
    j--;
  }
  if (i > 0) {
    return true;
  } else {
    return false;
  }
}


/**
 *
 * @param [in] fd file descriptor of input file
 * @param [out] output file of result bredo generation
 * @return 0 if operation was successful
 */


size_t NumberSentence(unsigned char const *mp) {
  size_t k = 0;
  size_t i = 0;
  while (mp[i] != '\0') {
    if (mp[i] == '\n') {
      k++;
    }
    i++;
  }
  return k;
}

//!
//! \param fd - input file
//! \param strings sentence from input file
//! \return number of sentence
size_t SpliteToSentence(int fd, unsigned char ***strings) {
  struct stat statbuf = {0};
  if (fstat(fd, &statbuf) < 0) /* определить размер входного файла */
    printf("fstat error");

  int *map = (int *) mmap(0, (size_t) (statbuf.st_size + 1), PROT_READ, MAP_SHARED, fd, 0);
  if (map == MAP_FAILED) {
    close(fd);
    perror("Error mmapping the file");
    exit(EXIT_FAILURE);
  }
  unsigned char *mp = (unsigned char *) map;

  size_t buff_size = NumberSentence(mp) + 1;
  *strings = (unsigned char **) calloc(buff_size, sizeof(unsigned char *));
  (*strings)[0] = mp;

  int j = 1;
  unsigned char enter = '\n';

  for (size_t i = 0; i <= statbuf.st_size; i++) {
    if (mp[0] == enter) {
      (*strings)[j] = mp + 1;
      j++;
      mp++;
    } else {
      mp = mp + 1;
    }
  }
  return buff_size;
}


